section .data
section .bss
	punteroR1:
		resb 4
	punteroR2:
		resb 4
	punteroG1:
		resb 4
	punteroG2:
		resb 4
	punteroB1:
		resb 4
	punteroB2:
		resb 4
section	.text
    global asmPrint	;must be declared for linker (ld)
	global blendAsm

asmPrint: ;tell linker entry point
	push ebp ;enter 0,0
	mov ebp,esp ;enter 0,0
	mov	ecx, [EBP+8];message
	mov	edx, [EBP+12];message length
	mov	ebx,1	;file descriptor (stdout)
	mov	eax,4	;system call number (sys_write)
	int	0x80	;call kernel
	mov ebp,esp ;Reset Stack  (leave)
	pop ebp ;Restore old EBP  (leave)	
	ret

blendAsm: ;linkey entry point al blend
	push ebp ;enter 0,0
	mov ebp,esp ;enter 0,0
	mov edx, [EBP+12]
	mov [punteroR1], edx
	mov edx, [EBP+16]
	mov [punteroG1], edx
	mov edx, [EBP+20]
	mov [punteroB1], edx
	mov edx, [EBP+24]
	mov [punteroR2], edx
	mov edx, [EBP+28]
	mov [punteroG2], edx
	mov edx, [EBP+32]
	mov [punteroB2], edx
	mov edx, [EBP+8] ;length
	mov ecx, edx
	inc ecx
	mov esi,-1
	call iteratorCheck
	mov ebp,esp ;Reset Stack  (leave)
	pop ebp ;Restore old EBP  (leave)	
	ret
iteratorCheck:
	dec ecx
	inc esi
	cmp ecx, 0
	jne mulBlend
	;jne aclarar
	ret
mulBlend:
	mov edx, [punteroR1]
	mov eax, [edx+4*esi] 
	mov edx, [punteroR2]
	mov ebx, [edx+4*esi] 
	mul ebx
	mov ebx , 255
	div ebx
	mov edx, [punteroR1]
	mov [edx+4*esi], eax

	mov edx, [punteroG1]
	mov eax, [edx+4*esi] 
	mov edx, [punteroG2]
	mov ebx, [edx+4*esi] 
	mul ebx
	mov ebx , 255
	div ebx
	mov edx, [punteroG1]
	mov [edx+4*esi], eax

	mov edx, [punteroB1]
	mov eax, [edx+4*esi] 
	mov edx, [punteroB2]
	mov ebx, [edx+4*esi] 
	mul ebx
	mov ebx , 255
	div ebx
	mov edx, [punteroB1]
	mov [edx+4*esi], eax
	jmp iteratorCheck

aclarar:
	mov edx, [punteroR1]
	mov eax, [edx+4*esi] 
	add eax, 15
	mov [edx+4*esi], eax

	mov edx, [punteroG1]
	mov eax, [edx+4*esi] 
	add eax, 15
	mov [edx+4*esi], eax

	mov edx, [punteroB1]
	mov eax, [edx+4*esi] 
	add eax, 15
	mov [edx+4*esi], eax
	jmp iteratorCheck